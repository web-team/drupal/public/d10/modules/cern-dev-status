# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [3.1.2] - 08/01/2024

- previous error was still present on some cases, fixed used [https://www.drupal.org/node/3158256](https://www.drupal.org/node/3158256) but adding sting id.
- fix `Uncaught TypeError: $(...).cookie is not a function` usign [https://www.drupal.org/node/3104677](https://www.drupal.org/node/3104677)


## [3.1.1] - 14/11/2023

- fix `Uncaught TypeError: $(...).once is not a function` using [https://www.drupal.org/node/3158256](https://www.drupal.org/node/3158256)

## [3.1.0] - 14/9/2023

- updated checkCernDevStatus() Argument #1 ($event) must be of type Symfony\Component\HttpKernel\Event\FilterResponseEvent
  
## [2.0.6] - 25/11/2021

- Add `core_version_requirement: ^9 || ^10` and remove `core: 8.x` from composer

## [2.0.5] - 8/2/2021

- Remove deprecated function drupal_set_message (passes d9-readiness)

## [2.0.4] - 8/2/2021

- Add core: 8.x to fix enabling issue

## [2.0.3] - 13/01/2021

- Update module to be D9-ready

## [2.0.2] - 4/12/2020

- Add composer.json file

## [2.0.1] - 18/07/2018

- Fixed issue of on uninstall module page

## [2.0.0]

- Initial version of Drupal 8 module
